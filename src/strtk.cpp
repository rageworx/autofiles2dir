#ifdef _WIN32
#include <windows.h>
#endif

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <string>

#include "strtk.h"

PSTRING cUT( const char* src )
{
    if ( src == NULL )
        return NULL;

    if ( strlen(src)>1024 )
        return NULL;

    static char srcdup[1024] = {0};

    memset( srcdup, 0, 1024 );
    memcpy( srcdup, src, strlen( src ) );

#ifdef UNICODE
    char  ftarget[]   = "%s";
    char  freplace    = 'S';
#else
    char  ftarget[]   = "%S";
    char  freplace    = 's';
#endif

    char* fnd = strstr( srcdup, ftarget );
    
    while( fnd != NULL )
    {
        fnd++;
        *fnd = freplace;
        fnd = strstr( fnd, ftarget );
    }

#ifdef UNICODE
    static wchar_t wcharconv[1024] = {0};
    mbstowcs( wcharconv, srcdup, 1024 );
    return wcharconv;
#else
    return srcdup;
#endif
}

STDSTRING StringToUpper( STDSTRING str )
{
    STDSTRING retstr = str;

    std::transform( retstr.begin(), 
                    retstr.end(), 
                    retstr.begin(), 
                    ::toupper );

    return retstr;
}
