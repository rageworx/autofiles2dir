#ifndef __STRTOOLKIT_H__
#define __STRTOOLKIT_H__

#include "platfdep.h"

STDSTRING StringToUpper( STDSTRING str );
PSTRING cUT( const char* src );

#endif /// __STRTOOLKIT_H__
