#ifndef __FILETOOLS_H__
#define __FILETOOLS_H__

#include "platfdep.h"

#include <vector>

int existsdir( const PSTRING dname );
int makedir( const PSTRING dname );
int removedir( const PSTRING dname );
int dirlistfile( const PSTRING dir, std::vector< STDSTRING > &filelist );
int dirlistdir( const PSTRING dir, std::vector< STDSTRING > &dirlist );

int copyfile( const PSTRING from, const PSTRING to, bool ow = false );
int movefile( const PSTRING from, const PSTRING to, bool ow = false );
int removefile( const PSTRING fname );

#endif /// of __FILETOOLS_H__
