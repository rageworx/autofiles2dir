/*****************************************************************
 * AutoFiles2Dir
 * --------------------------------------------------------------
 * (C)2019 Raphael Kim, for platform-less.
 *****************************************************************/
#ifdef _WIN32
#include <windows.h>
#endif

#include "af2d.h"
#include "af2dproc.h"
#include "strtk.h"

#include <iostream>

//////////////////////////////////////////////////////////////////

using namespace std;

//////////////////////////////////////////////////////////////////

#define PROGRAM_VERSION         _UT("0.6.4.36")

static STDSTRING opt_me = _UT("af2d");
static STDSTRING opt_search_dir;

static bool      opt_merge_dir = false;
static bool      opt_ask_job   = false;
static bool      opt_off_verb  = false;
static bool      opt_full_auto = true;
static bool      opt_copy_only = false;

//////////////////////////////////////////////////////////////////

STDSTRING stripPath( PSTRING src )
{
    STDSTRING retstr = src;

    // find stripping path.
    size_t fpos = retstr.find_last_of( _UT( "/" ) );
    if ( fpos == STDSTRING::npos )
    {
        fpos = retstr.find_last_of( _UT( "\\" ) );
    }

    if ( fpos != STDSTRING::npos )
    {
        retstr = retstr.substr( fpos + 1 );
    }

    return retstr;
}

bool parseArgvs( int argc, PSTRING* argv )
{
    if ( ( argc <= 0 ) || ( argv == NULL ) ) /// ???
        return false;

    opt_me = stripPath( argv[0] );
    
    if ( argc == 1 )
        return false;

    for( int cnt=1; cnt<argc; cnt++ )
    {
        STDSTRING param = argv[cnt];

        if ( param == _UT("-a") )
        {
            opt_full_auto = true;
        }
        else
        if ( param == _UT("-n") )
        {
            opt_off_verb = true;
        }
        else
        if ( param == _UT("-m") )
        {
            opt_merge_dir = true;
        }
        else
        if ( param == _UT("-k") )
        {
            opt_ask_job = true;
        }
        else
        if ( param == _UT("-c") )
        {
            opt_copy_only = true;
        }
        else
        if ( opt_search_dir.size() == 0 )
        {
            opt_search_dir = argv[cnt];
        }
    }

    return true;
}

void printTitle()
{
    STRINGCHAR strbi[80] = {0};

#if defined(__GNUC__) || defined(__llvm__)
#   if defined(_WIN32)
#       ifdef __amd64__
#			define  PLFT	_UT("Windows x86.64")
#			define	GCCT	_UT("MinGW-W64 x86.64 g++")
#       else
#			define	PLFT	_UT("windows x86.32")
#			define	GCCT	_UT("MinGW-W64 x86.32 g++")
#       endif
#   elif defined(__linux__)
#		if defined(__amd64__)
#			define  PLFT	_UT("linux x86.64")
#			define  GCCT	_UT("linux-x86.64-g++")
#		elif defined(__x8632__)
#			define  PLTF	_UT("linux x86.32")
#			define  GCCT	_UT("linux-x86.32-g++")
#		elif defined(__aarch64__)
#			define  PLTF	_UT("arm-aarch64")
#			define	GCCT	_UT("linux-aarch64-g++")
#		elif defined(__arm__)
#			define	PLTF	_UT("arm system")
#			define  GCCT	_UT("linux-arm-g++")
#		else
#			define  PLTF	_UT("linux system")
#			define	GCCT	_UT("linux-g++")
#		endif
#   elif defined(__APPLE__)
#		if defined(__x86_64__)
#			define  PLTF	_UT("apple-x86.64")
#			define  GCCT	_UT("apple-clang-x86.64-g++")
#		else
#			define  PLTF	_UT("apple system");
#			define	GCCT	_UT("apple-clang-g++")
#		endif
#   else
#			define	GCCT	_UT("g++");
#   endif
#else
#	define PLTF		_UT("unknown system")
#	define GCCT		_UT("unknown compiler")
#endif
    SPRINTF( strbi, 
             _UT("%s version %u.%u.%u"),
             GCCT,
             __GNUC__,
             __GNUC_MINOR__,
             __GNUC_PATCHLEVEL__ );

    PRINTF( cUT("%s : Automatic similar filenames into one directory tool on %s\n"),
            opt_me.c_str(),
			PLTF );
    PRINTF( cUT("Build info: app version %s with %s\n"),
            PROGRAM_VERSION,
            strbi );
    PRINTF( _UT("(C)2019, Raphael Kim.\n") );
    PRINTF( _UT("\n") );
}

void printHelp()
{
    PRINTF( _UT("   usage:\n") );
    PRINTF( cUT("       %s (options) [searching directory]\n"),
            opt_me.c_str() );
    PRINTF( _UT("\n") );
    PRINTF( _UT("   options:\n") );
    PRINTF( _UT("       -a    : fully automatic (default: on)\n") );
    PRINTF( _UT("       -n    : verbose off (default: off)\n") );
    PRINTF( _UT("       -m    : enable merge in directory (default: off)\n") );
    PRINTF( _UT("       -k    : ask before any job (default: off)\n") );
    PRINTF( _UT("       -c    : do not move file, instead copy\n") );
    PRINTF( _UT("\n") );
}

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

#ifdef _WIN32
void procAutoLocale()
{
    const char* convLoc =NULL;

    LANGID currentUIL = GetSystemDefaultLangID();

    switch( currentUIL & 0xFF )
    {
        case LANG_KOREAN:
            convLoc = "korean";
            break;

        case LANG_JAPANESE:
            convLoc = "japanese";
            break;

        case LANG_CHINESE:
            convLoc = "chinese";
            break;

        case LANG_CHINESE_TRADITIONAL:
            convLoc = "chinese_traditional";
            break;

        default:
            convLoc = "C";
            break;
    }

    setlocale( LC_ALL, convLoc );
}
#endif

#ifdef _WIN32
int wmain( int argc, wchar_t** argv)
#else
int main( int argc, char** argv )
#endif /// of _WIN32
{
#ifdef _WIN32
    procAutoLocale();
#endif 
    
    bool bopts = parseArgvs( argc, argv );

    printTitle();

    if ( bopts == false )
    {
        printHelp();
        return 0;
    }

    AF2DProc* af2dp = new AF2DProc( opt_search_dir.c_str() );
    if ( af2dp != NULL )
    {
        // apply options.
        af2dp->OptionCopyNotMove( opt_copy_only );
        af2dp->OptionAskAllJobs( opt_ask_job );
        af2dp->OptionVerboseOff( opt_off_verb );
        af2dp->OptionMerge( opt_merge_dir );

        PRINTF( cUT("- Start to search dir: %s\n"),
               opt_search_dir.c_str() );

        af2dp->Search();

        PRINTF( _UT("- %u files and %u directories found.\n"),
                af2dp->Files(),
                af2dp->Dirs() );
        PRINTF( _UT("- Automatic matching files to each directories ...\n") );

        af2dp->AutoCollect();

        PRINTF( _UT("- Finalizing ...\n") );

        delete af2dp;

        PRINTF( _UT("- Completed.\n") );
    }

    return 0;
}

#ifdef UNSUPPORTED_WMAIN
extern int _CRT_glob;
extern "C" void __wgetmainargs(int*,wchar_t***,wchar_t***,int,int*);

int main()
{
    wchar_t** enpv;
    wchar_t** argv;
    int       argc = 0;
    int       si   = 0;

    __wgetmainargs( &argc, &argv, &enpv, _CRT_glob, &si );
    return wmain( argc, argv );
}
#endif /// of UNSUPPORTED_WMAIN
