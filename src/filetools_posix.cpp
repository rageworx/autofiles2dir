// POSIX may Linux or Apple Mac.
#if defined(__linux__)||defined(__APPLE__)
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <dirent.h>

#include <cstdio>
#include <cstdlib>

#include <iostream>
#include <fstream>

#include "filetools.h"

using namespace std;

int existsdir( const PSTRING dname )
{
    return access( dname, 0 );
}

int makedir( const PSTRING dname )
{
    return mkdir( dname, 0755 );
}

int removedir( const PSTRING dname )
{
    return rmdir( dname );
}

int dirlistfile( const PSTRING dir, std::vector< STDSTRING > &filelist )
{
    DIR* pdir = opendir( dir );
    int  reti = -1;

    if ( pdir != NULL )
    {
        struct dirent* dent = readdir( pdir );
        while( dent != NULL )
        {
            if ( dent->d_type & DT_REG )
            {
                STDSTRING fname = dent->d_name;
                filelist.push_back( fname );
                reti++;
            }
            dent = readdir( pdir );
        }
        
        closedir( pdir );
    }

    return reti;
}

int dirlistdir( const PSTRING dir, std::vector< STDSTRING > &dirlist )
{
    DIR* pdir = opendir( dir );
    int  reti = -1;

    if ( pdir != NULL )
    {
        struct dirent* dent = readdir( pdir );
        while( dent != NULL )
        {
            if ( dent->d_type & DT_DIR )
            {
                STDSTRING fname = dent->d_name;
                dirlist.push_back( fname );
                reti++;
            }
            dent = readdir( pdir );
        }

        closedir( pdir );
    }

    return reti;
   return -1;
}

int copyfile( const PSTRING from, const PSTRING to, bool ow )
{
    char cmd[1024] = {0};

    if ( ow == false )
    {
        sprintf( cmd,
                 "cp -n %s %s",
                 from,
                 to );
    }
    else
    {
        sprintf( cmd, 
                 "cp -f %s %s",
                 from,
                 to );
    }

    return system( cmd );

    /*
    ifstream srcf( from, ios::binary );
    ofstream dstf( to, ios::binary );

    dstf << srcf.rdbuf();
    
    int reti = -1;

    if ( dstf.tellp() == srcf.tellg() )
        reti = 0;

    srcf.close();
    dstf.close();

    return reti;
    */
}

int movefile( const PSTRING from, const PSTRING to, bool ow )
{
    char cmd[1024] = {0};

    if ( ow == false )
    {
        sprintf( cmd,
                 "mv -n %s %s",
                 from,
                 to );
    }
    else
    {
        sprintf( cmd, 
                 "mv -f %s %s",
                 from,
                 to );
    }

    return system( cmd );

    /*
    bool need2ow = false;

    // check destination 
    if ( access( to, 0 ) == 0 )
    {
        need2ow = true;
    }

    if ( need2ow == true )
    {
        if ( ow == false )
            return -10;

        if ( unlink( to ) != 0 )
            return -11;

        if ( copyfile( from, to ) == false )
            return -20;

        unlink( to );
    }

    return 0;
    */
}

int removefile( const PSTRING fname )
{
    if ( access( fname, 0 ) == 0 )
    {
        return unlink( fname );
    }

    return 0;
}


#endif /// of __linux__ or __APPLE__
