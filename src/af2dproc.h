#ifndef __AF2DPROC_H__
#define __AF2DPROC_H__

#include "platfdep.h"
#include <vector>
#include <string>

typedef struct
{
    int                 primaryindex;
    bool                collected;
    // kind types :
    //    0 = number
    //    1 = alphabet
    //    2 = other 
    std::vector< int >       kind;
    std::vector< STDSTRING > data;
}SimilarData;

typedef void (*progressCB)(unsigned curidx,unsigned maxidx);

class AF2DProc
{
    public:
        AF2DProc( const PSTRING start_dir );
        ~AF2DProc();

    public:
        void Search(); /// use it inside thread.
        void AutoCollect();

    public:
        size_t Files() { return listfile.size(); }
        size_t Dirs()  { return listdir.size(); }
        const PSTRING File( unsigned idx )
        { if ( idx < listfile.size() ) return listfile[ idx ].c_str(); return NULL; }
        const PSTRING Directory( unsigned idx )
        { if ( idx < listdir.size() ) return listdir[ idx ].c_str(); return NULL; }
        void SetProgressCB( progressCB cb = NULL )
        { progressc = cb; }
        void OptionCopyNotMove( bool b ) { optCopyNotMove = b; }
        void OptionAskAllJobs( bool b )  { optAskEachJobs = b; }
        void OptionVerboseOff( bool b )  { optVerboseOff = b; }
        void OptionMerge( bool b )       { optMerge = b; }

    protected:
        void clearlists( int ct = 0 );
        void findsimilar();

    protected:
        progressCB               progressc;
        STDSTRING                rootdir;
        std::vector< STDSTRING > listfile;
        std::vector< STDSTRING > listdir;

    protected:
        std::vector< SimilarData >        sepdata;
        std::vector< std::vector< int > > listcollect;

    private:
        bool optCopyNotMove;
        bool optAskEachJobs;
        bool optVerboseOff;
        bool optMerge;
};

#endif /// of __AF2DPROC_H__
