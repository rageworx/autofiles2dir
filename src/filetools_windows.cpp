#if defined(_WIN32)

#if !defined(UNICODE)
#   define UNICODE
#endif

#if !defined(_UNICODE)
#   define _UNICODE
#endif

#include <unistd.h>
#include <windows.h>
#include <shlwapi.h>
#include <shellapi.h>
#include <wchar.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <filesystem>

#include "filetools.h"

int existsdir( const PSTRING dname )
{
    if ( PathFileExistsW( dname ) == TRUE )
    {
        return 0;
    }

    return -1;
}

int makedir( const PSTRING dname )
{
    if ( PathFileExistsW( dname ) == TRUE )
        return -1;

    if ( CreateDirectoryW( dname, NULL ) == FALSE )
        return -2;

    return 0;
}

int removedir( const PSTRING dname )
{
    if ( PathFileExistsW( dname ) == TRUE )
    {
        if ( RemoveDirectoryW( dname ) == TRUE )
            return 0;
    }

    return -1;
}

void makedirsearchstring( STDSTRING &dir )
{
    if ( dir.size() < 2 )
        return;

    if ( dir[ dir.size() - 1 ] == L'\\' )
    {
        dir += L"*";
    }
    else
    if ( dir[ dir.size() - 1 ] != L'*' )
    {
        dir += L"\\*";
    }
}

int dirlistfile( const PSTRING dir, std::vector< STDSTRING > &filelist )
{
    if ( wcslen( dir ) < 2 )
        return -1;

    STDSTRING dirdst = dir;

    makedirsearchstring( dirdst );

    WIN32_FIND_DATAW fdt;

    HANDLE hFind = FindFirstFileW( dirdst.c_str(), &fdt );

    if ( hFind != INVALID_HANDLE_VALUE )
    {
        filelist.clear();

        while( true )
        {
            if ( fdt.dwFileAttributes & FILE_ATTRIBUTE_ARCHIVE )
            {
                STDSTRING fname = fdt.cFileName;
                if ( ( fname != L"." ) && ( fname != L".." ) )
                {
                    filelist.push_back( fname );
                }
            }

            if ( FindNextFileW( hFind, &fdt ) == FALSE )
                break;
        }

        FindClose( hFind );
    }

    return 0;
}

int dirlistdir( const PSTRING dir, std::vector< STDSTRING > &dirlist )
{
    if ( wcslen( dir ) < 2 )
        return -1;

    STDSTRING dirdst = dir;

    makedirsearchstring( dirdst );

    WIN32_FIND_DATAW fdt;

    HANDLE hFind = FindFirstFileW( dirdst.c_str(), &fdt );

    if ( hFind != INVALID_HANDLE_VALUE )
    {
        dirlist.clear();

        while( true )
        {
            if ( fdt.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
            {
                STDSTRING fname = fdt.cFileName;
                if ( ( fname != L"." ) && ( fname != L".." ) )
                {
                    dirlist.push_back( fname );
                }
            }

            if ( FindNextFileW( hFind, &fdt ) == FALSE )
                break;
        }

        FindClose( hFind );
    }

    return 0;

}

int copyfile( const PSTRING from, const PSTRING to, bool ow )
{
    DWORD cpopt = COPY_FILE_FAIL_IF_EXISTS;

    if ( ow == true )
    {
        cpopt = 0;
    }

    BOOL reb = CopyFileExW( from, 
                            to,
                            NULL,
                            NULL,
                            FALSE,
                            cpopt );

    if ( reb == TRUE )
    {
        return 0;
    }

    return -1;
}

int movefile( const PSTRING from, const PSTRING to, bool ow )
{
    DWORD cpopt = 0;

    if ( ow == true )
    {
        cpopt = MOVEFILE_REPLACE_EXISTING;
    }

    BOOL reb = MoveFileExW( from, 
                            to,
                            cpopt );

    if ( reb == TRUE )
    {
        return 0;
    }

    return -1;
}


int removefile( const PSTRING fname )
{
    if ( DeleteFileW( fname ) == FALSE )
        return -1;

    return 0;
}

#endif /// of _WIN32
