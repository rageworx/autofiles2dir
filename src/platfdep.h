#ifndef __PLATFORM_DEPENDANCY_H__
#define __PLATFORM_DEPENDANCY_H__

#if defined(_WIN32)
#   if !defined(UNICODE)
#       define UNICODE
#   endif
#   if !defined(_UNICODE)
#       define _UNICODE
#   endif
#   define STDSTRING        std::wstring
#   define STRINGCHAR       wchar_t
#   define PSTRING          wchar_t*
#   define _UT(x)           L ## x
#   define PRINTF           wprintf
#   define STRCPY           wcscpy
#   define SPRINTF          wsprintf
#   define _PS_             "%S"
#else
#   define STDSTRING        std::string
#   define STRINGCHAR       char
#   define PSTRING          char*
#   define _UT(x)           x
#   define PRINTF           printf
#   define STRCPY           strcpy
#   define SPRINTF          sprintf
#   define _PS_             "%s"
#endif

#endif /// of __PLATFORM_DEPENDANCY_H__
