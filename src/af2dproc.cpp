#include "af2dproc.h"
#include "filetools.h"
#include "strtk.h"

#include <algorithm>

using namespace std;

////////////////////////////////////////////////////////////////////

//#define  AF2D_DEBUG

////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////

static bool isDigit( const STRINGCHAR c )
{
    bool res = true;
    switch( c )
    {
        case _UT('0'): 
        case _UT('1'): 
        case _UT('2'): 
        case _UT('3'): 
        case _UT('4'):
        case _UT('5'): 
        case _UT('6'): 
        case _UT('7'): 
        case _UT('8'): 
        case _UT('9'):
            break;

        default:
            res = false;
            break;
    }

    return res;
}

static bool isOther( const STRINGCHAR c )
{
    bool res = true;

    switch( c )
    {
        case _UT('_'):
        case _UT('~'):
        case _UT('@'):
        case _UT('$'):
        case _UT('%'):
        case _UT('^'):
        case _UT('&'):
        case _UT('*'):
        case _UT('('):
        case _UT(')'):
        case _UT('-'):
        case _UT('='):
        case _UT('+'):
        case _UT('['):
        case _UT(']'):
        case _UT('{'):
        case _UT('}'):
        case _UT('|'):
            break;

        default:
            res = false;
            break;
    }

    return res;
}

static bool seperating( STDSTRING &src, SimilarData &out )
{
    if ( src.size() == 0 )
        return false;

    out.primaryindex = -1;
    out.collected = false;

    // -1 : unknown
    // 0  : numberic
    // 1  : words ...
    int seq = -1;

    STDSTRING tmpsrc = src;
    STDSTRING words;

    // changes all others to space.
    for( size_t cnt=0; cnt<tmpsrc.size(); cnt++ )
    {
        if ( isOther( tmpsrc[cnt] ) == true )
        {
            tmpsrc[cnt] = _UT(' ');
        }
    }

    // remove spaces ...
    size_t cntmx = tmpsrc.size();
    size_t cntq  = 0;
    size_t cnts  = 0;
    while( cntq <= cntmx )
    {
        if ( tmpsrc[cnts] == _UT(' ') )
        {
            tmpsrc.erase( tmpsrc.begin() + cnts );
        }
        else
        {
            cnts++;
        }
        cntq++;
   }

    if ( isDigit( src[0] ) == true )
    {
        seq = 0;
    }
    else
    {
        seq = 1;
    }

    for( size_t cnt=0; cnt<tmpsrc.size(); cnt++ )
    {
        bool bdigit = false;
        bdigit = isDigit( tmpsrc[cnt] );

        if ( ( seq != 0 ) && ( bdigit == true ) )
        {
            if ( words.size() > 0 )
            {
                // check primary indexed 
                if ( out.primaryindex < 0 )
                {
                    // bigger than 2 charactors may primary.
                    if ( words.size() > 2 )
                    {
                        out.primaryindex = out.data.size();
                    }
                }
                out.kind.push_back( 1 );
                out.data.push_back( words );
                words.clear();
            }

            seq = 0;
            words += tmpsrc[cnt];
        }
        else
        if ( ( seq != 1 ) && ( bdigit == false ) )
        {
            if ( words.size() > 0 )
            {
                out.kind.push_back( 0 );
                out.data.push_back( words );
                words.clear();
            }

            seq = 1;
            words += tmpsrc[cnt];
        }
        else
        {
            words += tmpsrc[cnt];
        }
    }

    if ( words.size() > 0 )
    {
        out.kind.push_back( seq );
        out.data.push_back( words );
    }

    // check primary index again if failed to find before.
    if ( out.primaryindex < 0 )
    {
        for( size_t cnt=0; cnt<out.data.size(); cnt++ )
        {
            // numberic string cannot be primary ..
            if ( out.kind[cnt] > 0 )
            {
                if ( out.data[cnt].size() > 1 )
                {
                    out.primaryindex = cnt;
                    break;
                }
            }
        }
    }

    return true;
}

static void filelists2similardata( vector< STDSTRING > &src, vector< SimilarData > &out )
{
    for( size_t cnt=0; cnt<src.size(); cnt++ )
    {
        SimilarData tdata;

        seperating( src[cnt], tdata );

        out.push_back( tdata );
    }
}

////////////////////////////////////////////////////////////////////

AF2DProc::AF2DProc( const PSTRING start_dir )
 : progressc( NULL ),
   optCopyNotMove( false ),
   optAskEachJobs( false ),
   optVerboseOff( false ),
   optMerge(false)
{
    rootdir = start_dir;
}

AF2DProc::~AF2DProc()
{
    clearlists();
}

void AF2DProc::AutoCollect()
{
    findsimilar();

    if ( listcollect.size() > 0 )
    {
        for( size_t cnt=0; cnt<listcollect.size(); cnt++ )
        {
            if ( listcollect[cnt].size() > 1 )
            {
                size_t cidx = listcollect[cnt][0];
                int    iidx = sepdata[cidx].primaryindex;

                if ( iidx < 0 )
                {
                    iidx = 0;
                }

                STDSTRING dirname = sepdata[cidx].data[iidx];

                if ( iidx < sepdata[cidx].data.size() )
                {
                    dirname += _UT("-");
                    dirname += sepdata[cidx].data[iidx+1];
                }
               
                STDSTRING combinedname = rootdir;
#ifdef _WIN32
                combinedname += _UT("\\");
#else
                combinedname += _UT("/");
#endif
                combinedname += dirname;
                
                PRINTF( cUT("- Generating directory : %s\n"),
                        combinedname.c_str() );

                if ( existsdir( combinedname.c_str() ) < 0 )
                {
                    if ( makedir( combinedname.c_str() ) < 0 )
                    {
                        PRINTF( cUT("\n\tError: new directory failure - %s"),
                                combinedname.c_str() );
                    }
                }

                for( size_t lp=0; lp<listcollect[cnt].size(); lp++)
                {
                    size_t midx = listcollect[cnt][lp];

                    STDSTRING pathsrc = rootdir;
                    STDSTRING pathdst = combinedname;

#ifdef _WIN32
                    pathsrc += _UT("\\");
                    pathdst += _UT("\\");
#else
                    pathsrc += _UT("/");
                    pathdst += _UT("/");
#endif
                    pathsrc += listfile[midx];
                    pathdst += listfile[midx];

                    int reti = -1;

#ifdef AF2D_DEBUG
                    PRINTF( cUT("#Move file : [%s] to [%s]\n"),
                            pathsrc.c_str(),
                            pathdst.c_str() );
#else
                    if ( optCopyNotMove == true )
                    {
                        reti = copyfile( pathsrc.c_str(), pathdst.c_str() );
                    }
                    else
                    {
                        reti = movefile( pathsrc.c_str(), pathdst.c_str() );
                    }

                    if ( reti < 0 )
                    {
                        PRINTF( _UT("Error: Failed to move/copy files!\n") );
                    }
#endif /// of AF2D_DEBUG
                }
            }
        }
    }
}

void AF2DProc::findsimilar()
{
    if ( listfile.size() == 0 )
        return;

    clearlists( 2 );

    filelists2similardata( listfile, sepdata );

#ifdef AF2D_DEBUG
    PRINTF( _UT("- Found %u files -> collecting %u items\n"), 
            listfile.size(),
            sepdata.size() );
#endif

    for( size_t cnt=0; cnt<sepdata.size(); cnt++ )
    {
        // check names with primary indexed data.
        SimilarData cdata = sepdata[cnt];
        vector< int > cltd;

        if ( cdata.collected == false )
        {
            STDSTRING basestr;
            STDSTRING secondstr;
            size_t   startidx;

            if ( cdata.primaryindex >= 0 )
            {
                startidx = cdata.primaryindex;
            }

            basestr = StringToUpper( cdata.data[startidx] );

            if ( ( startidx + 1 ) < ( cdata.data.size() - 1 ) )
            {
                secondstr = StringToUpper( cdata.data[startidx+1] );
            }

#ifdef AF2D_DEBUG
            PRINTF( cUT("# basestr : %s\n"), basestr.c_str() );
            if ( secondstr.size() > 0 )
            {
                PRINTF( cUT("# secondstr : %s\n"), secondstr.c_str() );
            }
#endif
            cltd.push_back( cnt );

            for( size_t cx=0; cx<sepdata.size(); cx++ )
            {
                unsigned matchcnt = 0;

                SimilarData sdata = sepdata[cx];

                if ( ( cx != cnt ) && ( sdata.collected == false ) )
                {
                    for( size_t cs=0; cs<sdata.data.size(); cs++ )
                    {
                        STDSTRING diffs = StringToUpper( sdata.data[cs] );
                        
                        if ( diffs.find( basestr ) != STDSTRING::npos )
                        {
                            matchcnt++;
                        }
                        else
                        if ( secondstr.size() > 0 )
                        {
                            if ( diffs.find( secondstr ) != STDSTRING::npos )
                            {
                                matchcnt++;
                            }
                        }

                        if ( matchcnt > 1 )
                        {
                            sepdata[cx].collected = true;
                            sdata.collected = true;
                            cltd.push_back(cx);
                            break;
                        }
                    }
                }
            }
           
#ifdef AF2D_DEBUG
            PRINTF( cUT("\tSimilar set of %u(%s) : %u/%u items.\n"),
                    cnt,
                    basestr.c_str(),
                    cltd.size(),
                    sepdata.size() );
            PRINTF( _UT("\t") );
            for(size_t xxx=0; xxx<cltd.size(); xxx++)
            {
                PRINTF( _UT("%u "), cltd[xxx] );
            }
            PRINTF( _UT("\n") );
#endif /// of AF2D_DEBUG
        }

        if ( cltd.size() > 0 )
        {
            listcollect.push_back( cltd );
        }
    }
}

void AF2DProc::Search()
{
    int retfc = dirlistfile( rootdir.c_str(), listfile );
    int retdc = dirlistdir( rootdir.c_str(), listdir );
}

void AF2DProc::clearlists( int ct )
{
    bool ct_1 = true;
    bool ct_2 = true;

    switch( ct )
    {
        case 1:
            ct_2 = false;
            break;

        case 2:
            ct_1 = false;
            break;
    }

    if ( ct_1 == true )
    {
        listfile.clear();
        listdir.clear();
    }

    if ( ct_2 == true )
    {
        if ( listcollect.size() > 0 )
        {
            for( size_t cnt=0; cnt<listcollect.size(); cnt++ )
            {
                listcollect[cnt].clear();
            }
            listcollect.clear();
        }

        if ( sepdata.size() > 0 )
        {
            for( size_t cnt=0; cnt<sepdata.size(); cnt++ )
            {
                sepdata[cnt].kind.clear();
                sepdata[cnt].data.clear();
            }
            sepdata.clear();
        }
    }
}
